#!/bin/sh
SERVER="<IP>"
MOUNT_CMD="sshfs kindle@${SERVER}:/srv/kindle_statusdisplay/server/mnt /mnt/us/statusdisplay/mnt"
UPDATE_CMD="scp 'kindle@${SERVER}:/srv/kindle_statusdisplay/kindle/*' /mnt/us/statusdisplay/"
