#!/bin/sh

. /mnt/us/statusdisplay/config.sh

while true; do
  if mount | grep /mnt/us/statusdisplay/mnt > /dev/null; then
    sh /mnt/us/statusdisplay/mnt/kindle.sh
  else
    mkdir -p /mnt/us/statusdisplay/mnt
    $MOUNT_CMD
  fi
done
