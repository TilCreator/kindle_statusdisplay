#!/bin/env python3
import jinja2
import sys
import os
from datetime import datetime


env = jinja2.Environment(loader=jinja2.FileSystemLoader('.'))
env.add_extension('hamlish_jinja.HamlishExtension')
env.trim_blocks = True
env.lstrip_blocks = True
env.hamlish_mode = 'debug'


def get_file_or(path, convert_lambda, fallback):
    try:
        return convert_lambda(open('server/mnt/tmp/bat', 'r').read())
    except OSError:
        return fallback


kindle_bat = get_file_or('server/mnt/tmp/bat', lambda x: int(x), '?')

data = {
    'date': datetime.now().strftime('%Y-%M-%D %H:%M:%S'),
    'statuses': {
        'Kindle': {
            'text': f'Bat: {kindle_bat}',
            'errors': [],
        },
        'Rose': {
            'text': 'IPv6 Prefix: ' + os.popen('ip a show dev eth0 | grep inet6 | grep global | grep -v "inet6 fda0:" | head -n 1 | cut -d" " -f6 | cut -d":" -f -4').read().strip('\n') + '/64',
            'errors': [],
        }
    }
}

if type(kindle_bat) is int and kindle_bat < 50:
    data['statuses']['Kindle']['errors'].append('battery low')

data['summary'] = {
    'status': False not in [len(status["errors"]) == 0 for status in data['statuses'].values()]
}

print(env.get_template(sys.argv[1]).render(data))
