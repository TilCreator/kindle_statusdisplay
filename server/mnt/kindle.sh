#!/bin/sh

lipc-set-prop -- com.lab126.powerd preventScreenSaver 1
/etc/init.d/cron stop
/etc/init.d/phd stop
/etc/init.d/cmd stop
#/etc/init.d/powerd stop  # Needed for getting bat level
/etc/init.d/tmd stop
/etc/init.d/webreaderd stop
killall -9 logger
kill -9 `pidof lifeguard`
/etc/init.d/browserd stop
/etc/init.d/pmond stop
/etc/init.d/framework stop

#while :;do (waitforkey|xargs -n 2 ./showstatus.sh);done &
eips -c

mkdir -p /mnt/us/statusdisplay/mnt/tmp

while true; do
  if mount | grep /mnt/us/statusdisplay/mnt > /dev/null; then
    echo $(lipc-get-prop -- com.lab126.powerd status | grep "Battery Level" | cut -d" " -f3 | cut -d"%" -f1) > /mnt/us/statusdisplay/mnt/tmp/bat

    eips -g /mnt/us/statusdisplay/mnt/render/final.png

    sleep 3
  else
    echo "mount broke"
    $MOUNT_CMD
  fi
done
