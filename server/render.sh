#!/bin/sh
mkdir -p server/mnt/render
server/render_template.py server/template/index.haml > server/mnt/render/index.html

ln -srf server/template/static server/mnt/render/static
python -m http.server -d server/mnt/render -b localhost 8976 & HTTP_SERVER_PID=$!
firefox --screenshot server/mnt/render/raw.png http://localhost:8976/index.html --window-size=800,600
kill $HTTP_SERVER_PID

magick server/mnt/render/raw.png \
    -rotate 90 \
    -dither FloydSteinberg \
    -colorspace GRAY \
    -depth 8 \
    -colors 16 \
    -alpha off \
    -background "#FFFFFF" \
    -flatten \
    -normalize \
    server/mnt/render/final.png
