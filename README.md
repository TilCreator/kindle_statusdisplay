# Kindle Statusdisplay
This project uses a jaibroken Kindle and some linux system connected with sshfs to display some status. The server renders a haml jinja2 template with Python to a html file which is rendered to a png with Firefox which is rendered to 16 color grayscale with imagemagick. The kindle just mounts a directory on the server with sshfs and displays the final image (updates every 3 seconds).
## Setup
### Kindle
- Jailbreak the Kindle
- Install the usbnet hack for ssh and sshfs
- Copy the files in kindle to `/mnt/us/statusdisplay` on the kindle
- Copy `config.example.sh` to `config.sh` and change `SERVER` to the address of the server
- Generate a ssh key
### Server
- Install firefox, imagemagick, python-pip, ssh, sshfs and Python packages: jinja2 and hamlish_jinja
- Create user `kindle` with home `srv/kindle_statusdisplay`
- Clone this repo into `srv/kindle_statusdisplay` (make shure it's owned by `kindle`)
- Add the kindle to authorized_hosts for the kindle user
- Copy the .service and .timer files from server to `/etc/systemd/system`
### Start
- Enable and start the `statusdisplay.timer` on the server
- Start `/mnt/us/statusdisplay/main.sh` on the kindle and disown
## TODO
- Add some autostart on the Kindle
